#pragma once

#include "OutStream.h"
#include "FileStream.h"
#include <stdio.h>

 class Logger
{
	OutStream os;
	static FileStream* fs; //static pointer to fileStrem obj (in order to write to same file)
	bool _to_screen;
	static int _row_num;
	public:
		Logger(char* filename, bool logToScreen);
		~Logger();
		void print(char* msg);
};
