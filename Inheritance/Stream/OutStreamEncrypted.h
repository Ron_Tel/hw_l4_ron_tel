#pragma once

#include "OutStream.h"

class OutStreamEncrypted : public OutStream
{
	private:
		int _offset; //offest to move all charecters
	public:
		OutStreamEncrypted(int offset);
		~OutStreamEncrypted();
		OutStreamEncrypted& operator<<(char* str);
		OutStreamEncrypted& operator<<(int num);
		OutStreamEncrypted& operator<<(char*(*pf)());
};
